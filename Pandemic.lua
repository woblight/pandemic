local ST
if Pandemic == nil then 
    Pandemic = {}
end
ST = Pandemic

setmetatable(ST, {__index = getfenv() })
setfenv(1, ST)

RULES = [[
    Pandemic Rules:
        1) If you get infected you have to delete your character and restart
        2) You get infected by:
            - getting melee damaged from an NPC
                * pets are immune
                * anything that prevents damage protects you from the infection
            - entering a major city
            - someone in your team gets infected
        3) You may not form group with people above 5 level difference
]]

local eventHandler = CreateFrame("frame")
eventHandler:RegisterEvent("COMBAT_LOG_EVENT_UNFILTERED")
eventHandler:RegisterEvent("ADDON_LOADED")
eventHandler:RegisterEvent("PLAYER_ENTERING_WORLD")
eventHandler:RegisterEvent("ZONE_CHANGED_NEW_AREA")
eventHandler:RegisterEvent("ZONE_CHANGED")
eventHandler:RegisterEvent("TIME_PLAYED_MSG")
eventHandler:RegisterEvent("GROUP_ROSTER_UPDATE")
eventHandler:RegisterEvent("GROUP_JOINED")

local loseSplash = EFrame.Item()
loseSplash.anchorCenter = EFrame.root.center
loseSplash.width = 1024
loseSplash.height = 512
loseSplash.img0 = EFrame.Image(loseSplash)
loseSplash.img0.anchorTopLeft = loseSplash.topLeft
loseSplash.img0.height = 512
loseSplash.img0.width = 512
loseSplash.img0.source = "Interface\\AddOns\\Pandemic\\youlose-0"
loseSplash.img1 = EFrame.Image(loseSplash)
loseSplash.img1.anchorTopRight = loseSplash.topRight
loseSplash.img1.source = "Interface\\AddOns\\Pandemic\\youlose-1"
loseSplash.img1.height = 512
loseSplash.img1.width = 512
loseSplash.anim = EFrame.PropertyAnimation(loseSplash)
loseSplash.anim.property = "scale"
loseSplash.anim.from = 1
loseSplash.anim.to = 1.66
loseSplash.anim.duration = 4
loseSplash.visible = false

local ordered_log
function playerDeath(source)
    print(format("You got infected by %s!", source))
    local level = UnitLevel("player") + UnitXP("player")/UnitXPMax("player")
    local race, class = UnitRace("player"), UnitClass("player")
    local cr = format("%s %s", class, race)
    if not PandemicSettings.maxLevel[cr] or PandemicSettings.maxLevel[cr] < level then
        PandemicSettings.maxLevel[cr] = level
        if not PandemicSettings.maxLevelClass or not PandemicSettings.maxLevel[PandemicSettings.maxLevelClass] or PandemicSettings.maxLevel[PandemicSettings.maxLevelClass] < level then
            PandemicSettings.maxLevelClass = cr
        end
    end
    print(format("Level %.03f (%s) [Class Record %.03f] [Overall %.03f (%s)]", level, cr, PandemicSettings.maxLevel[cr], PandemicSettings.maxLevel[PandemicSettings.maxLevelClass], PandemicSettings.maxLevelClass))
    loseSplash.visible = true
    loseSplash.anim.running = true
    loseSplash.count = 0
    if not PandemicSettings.guids[UnitGUID("player")] then
        PandemicSettings.guids[UnitGUID("player")] = {}
    end
    local d = PandemicSettings.guids[UnitGUID("player")]
    d.name = UnitName("player")
    d.level = level
    d.class = class
    d.race = race
    d.source = source
    d.sex = UnitSex("player")
    d.time = time()
    ordered_log = nil
    refreshLog()
    RequestTimePlayed()
    PlaySoundFile("Interface\\Addons\\Pandemic\\youlose.ogg", "master")
end

function eventHandler.TIME_PLAYED_MSG(time)
    PandemicSettings.guids[UnitGUID("player")].played = time
    refreshLog()
end

local function processCombatEvent(...)
    local timestamp, subevent, _, sourceGUID, sourceName, sourceFlags, sourceRaidFlags, destGUID, destName, destFlags, destRaidFlags = ...
    local pguid = UnitGUID("player")
    local valid = destGUID == pguid
    for i=1,GetNumSubgroupMembers() do
        valid = valid or UnitGUID("party"..i) == destGUID
    end
    for i=1,GetNumGroupMembers() do
        valid = valid or UnitGUID("raid"..i) == destGUID
    end
    if not valid or PandemicSettings.guids[pguid] and PandemicSettings.guids[pguid].time or not strmatch(sourceGUID, "Creature") then return end
    if subevent == "SWING_DAMAGE" then
        local ammount = select(12, ...)
        if ammount > 0 then
            playerDeath(sourceName)
        end
    end
end

function eventHandler.COMBAT_LOG_EVENT_UNFILTERED()
    processCombatEvent(CombatLogGetCurrentEventInfo())
end

local forbidden = {
    [1453] = true,
    [1454] = true,
    [1455] = true,
    [1456] = true,
    [1457] = true,
    [1458] = true,
}

function eventHandler.ZONE_CHANGED_NEW_AREA()
    if forbidden[C_Map.GetBestMapForUnit("player")] then
        playerDeath("City's virulence")
    end
end

eventHandler.PLAYER_ENTERING_WORLD = eventHandler.ZONE_CHANGED_NEW_AREA
eventHandler.ZONE_CHANGED = eventHandler.ZONE_CHANGED_NEW_AREA

function eventHandler.GROUP_ROSTER_UPDATE()
    for i=1,GetNumSubgroupMembers() do
        if UnitLevel("party" .. i) - UnitLevel("player") > 5 then
            playerDeath("Grouping with " .. UnitName("party" .. i))
        end
    end
    for i=1,GetNumGroupMembers() do
        if UnitLevel("raid" .. i) - UnitLevel("player") > 5 then
            playerDeath("Raiding with " .. UnitName("raid" .. i))
        end
    end
end
eventHandler.GROUP_JOINED = eventHandler.GROUP_ROSTER_UPDATE

local defaultSettings = {
    fontSize = 18,
}

function eventHandler.ADDON_LOADED(addon)
    if addon == "Pandemic" then
        if not _G.PandemicSettings then
            _G.PandemicSettings = {
                guids = {},
                maxLevel = {},
                version = 0.1
            }
        end
        if not _G.PandemicSettings.version then
            for k, v in pairs(PandemicSettings.guids) do
                v.class, v.race = v.race, v.class
                _G.PandemicSettings.version = 0.1
            end
        end
            
        setmetatable(_G.PandemicSettings, {__index = defaultSettings})
    end
end

eventHandler:SetScript("OnEvent", EFrame:makeAtomic(function(self, event, ...) self[event](...) end))

local PandemicUI
EFrame = EFrame.Blizzlike
function showUI()
    if not PandemicUI then
        PandemicUI = EFrame.Window()
        
        PandemicUI:attach("sortby")
        PandemicUI:attach("inverted")
        PandemicUI:attach("textOnly")
        PandemicUI.sortby = "time"
        
        PandemicUI.title = "Pandemic"
        PandemicUI.width = EFrame.root.width * 0.50
        PandemicUI.height = EFrame.root.height * 0.66
        PandemicUI.centralItem = EFrame.ColumnLayout(PandemicUI)
        
        local bar = EFrame.RowLayout(PandemicUI.centralItem)
        
        local logBtn = EFrame.RadioButton(bar)
        logBtn.text = "log"
        logBtn.value = "log"
        logBtn.checked = true
        
        local rulesBtn = EFrame.RadioButton(bar)
        rulesBtn.text = "rules"
        rulesBtn.value = "rules"
        
        
        local bar2 = EFrame.RowLayout(PandemicUI.centralItem)
        
        local textOnly = EFrame.CheckButton(bar2)
        textOnly.text = "Text only"
        textOnly:connect("clicked", function() PandemicUI.textOnly = textOnly.checked end)
        
        EFrame.Label(bar2).text = "Text size:"
        
        local fontSize = EFrame.SpinBox(bar2)
        fontSize.from = 8
        fontSize.to = 72
        fontSize.value = PandemicSettings.fontSize
        fontSize:connect("valueModified", function(v) PandemicSettings.fontSize = v end)
        
        local sortRow = EFrame.RowLayout(bar2)
        sortRow.visible = EFrame.bind(logBtn, "checked")
        
        EFrame.Label(sortRow).text = "Sort by:"
        
        local function sortBtn(text, value)
            local btn = EFrame.Button(sortRow)
            btn.text = text
            btn.value = value
            btn:connect("clicked", function() if PandemicUI.sortby == value then PandemicUI.inverted = not PandemicUI.__inverted else PandemicUI.__inverted = false PandemicUI.sortby = value end end)
            return btn
        end
        
        local raceBtn = sortBtn("time", "time")
        local raceBtn = sortBtn("race", "race")
        local raceBtn = sortBtn("class", "class")
        local raceBtn = sortBtn("name", "name")
        local raceBtn = sortBtn("level", "level")
        local raceBtn = sortBtn("infected by", "source")
        
        local log = EFrame.TextArea(PandemicUI.centralItem)
        log.visible = EFrame.bind(logBtn, "checked")
        log.Layout.fillWidth = true
        log.Layout.fillHeight = true
        log.readOnly = true
        log.fontSize = EFrame.bind(fontSize, "value")
        log.text = EFrame.bind(function() return refreshLog() end)
        
        local rules = EFrame.TextArea(PandemicUI.centralItem)
        rules.visible = EFrame.bind(rulesBtn, "checked")
        rules.Layout.fillWidth = true
        rules.Layout.fillHeight = true
        rules.readOnly = true
        rules.text = RULES
        rules.fontSize = EFrame.bind(fontSize, "value")
    else
        PandemicUI.visible = not PandemicUI.visible
    end
end

local ctable = {}

local classIdx = {
    WARRIOR = 0,
    MAGE = 1,
    ROGUE = 2,
    DRUID = 3,
    HUNTER = 4,
    SHAMAN = 5,
    PRIEST = 6,
    WARLOCK = 7,
    PALADIN = 8
}

local classCoords = {}

for i = 1,12 do
    local info = C_CreatureInfo.GetClassInfo(i)
    if info then
        ctable[info.classFile] = info.className
    end
end
    
for k, v in pairs(classIdx) do
    local sx, sy = mod(v,4), floor(v/4)
    classCoords[ctable[k]] = format("%f:%f:%f:%f", sx/4 * 64 +1, (sx +1)/4 * 64 -1, sy/4 * 64 +1, (sy +1)/4 * 64 -1)
end

local raceIdx = {3, 7, 4, 6, 5, 8, 2, 1}

local raceCoords = {}

for i = 1, 8 do
    raceCoords[C_CreatureInfo.GetRaceInfo(raceIdx[i]).raceName] = {}
    for j = 2,3 do
        local idx = 8*(j-2) + i
        local sx, sy = mod(idx,4), floor(idx/4)
        raceCoords[C_CreatureInfo.GetRaceInfo(raceIdx[i]).raceName][j] = format("%f:%f:%f:%f", sx/4 * 64 +1, (sx +1)/4 * 64 -1, sy/4 * 64 +1, (sy +1)/4 * 64 -1)
    end
end

local last_order
function refreshLog()
    if not PandemicUI then return end
    textOnly, sortby, inverted = PandemicUI.textOnly, PandemicUI.sortby, PandemicUI.inverted
    local log = PandemicUI.log
    local stuff = {}
    if not ordered_log then
        ordered_log = {}
        for _, v in pairs(PandemicSettings.guids) do
            tinsert(ordered_log, v)
        end
    end
    sort(ordered_log, function(a,b) return last_order == sortby and a[sortby] < b[sortby] or inverted and a[sortby] > b[sortby] end)
    last_order = sortby
    for k, v in ipairs(ordered_log) do
        if v.time then
            if textOnly then
                stuff[k] = format("|cffcccccc[|cffffffff%s|r] |cffffd100 %s %s %s|r: level |cffffd100%.03f|r, infected by: |cffffd100%s|r", date(nil, v.time), v.race, v.class, v.name, v.level, v.source)
            else
                stuff[k] = format("|cffcccccc[|cffffffff%s|r] |TInterface\\Glues\\CharacterCreate\\UI-CharacterCreate-Races.blp:0::::64:64:%s:::|t|TInterface\\Glues\\CharacterCreate\\UI-CharacterCreate-Classes.blp:0::::64:64:%s:::|t |cffffd100%s|r: level |cffffd100%.03f|r, infected by: |cffffd100%s|r", date(nil, v.time), raceCoords[v.race][v.sex or 2], classCoords[v.class], v.name, v.level, v.source)
            end
        end
    end
    local t = ""
    for k, v in pairs(stuff) do
        t = t .. v .. "\n"
    end
    return t
end


local function chatcmd(msg)
    showUI()
end

_G.SLASH_PANDEMIC1 = "/pandemic"
SlashCmdList["PANDEMIC"] = chatcmd
